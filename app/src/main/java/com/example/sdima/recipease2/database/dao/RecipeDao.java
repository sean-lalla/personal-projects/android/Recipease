package com.example.sdima.recipease2.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.sdima.recipease2.pojos.Recipe;

import java.util.List;

@Dao
public interface RecipeDao {

    //Insert
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertRecipe(Recipe recipe);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    List<Long> insertRecipes(Recipe[] recipes);

    //Update
    @Update
    int updateRecipe(Recipe recipe);

    //Deletes
    @Delete
    void deleteRecipe(Recipe recipe);

    @Delete
    void deleteRecipes(Recipe[] recipes);

    @Query("DELETE FROM recipe")
    void deleteAll();

    //Queries
    @Query("SELECT * FROM recipe WHERE id = :id")
    LiveData<Recipe> getRecipeById(long id);

    @Query("SELECT * FROM recipe WHERE id in (:ids)")
    LiveData<List<Recipe>> getRecipesById(long[] ids);

    @Query("SELECT * FROM recipe")
    LiveData<List<Recipe>> getAllRecipes();

    @Query("SELECT * FROM recipe WHERE name LIKE '%' || :search || '%'")
    LiveData<List<Recipe>> getRecipesContainingName(String search);

    @Query("SELECT * FROM recipe WHERE name LIKE :search || '%'")
    LiveData<List<Recipe>> getRecipesStartingWithName(String search);

    @Query("SELECT DISTINCT R.id FROM recipe R JOIN ingredient_list IL ON IL.recipe_id = R.id WHERE IL.ingredient_id = :id")
    LiveData<List<Long>> getRecipesUsingIngredient(long id);
}
