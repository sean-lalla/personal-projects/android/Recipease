package com.example.sdima.recipease2.ui.newrecipe.ingredientadapter;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.sdima.recipease2.R;
import com.example.sdima.recipease2.enums.Measurement;
import com.example.sdima.recipease2.pojos.Ingredient;
import com.example.sdima.recipease2.pojos.IngredientList;
import com.example.sdima.recipease2.ui.newrecipe.NewRecipeFragmentViewModel;


import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IngredientAdapter extends ArrayAdapter<IngredientList> {

    private Context context;
    private List<IngredientList> ingredientLists;
    private List<Ingredient> ingredients;
    private NewRecipeFragmentViewModel mViewModel;


    public IngredientAdapter(@NonNull Context context,  @NonNull List<IngredientList> ingredientLists, @NonNull List<Ingredient> ingredients, @NonNull NewRecipeFragmentViewModel mViewModel) {
        super(context, R.layout.ingredient_item, ingredientLists);
        this.ingredientLists = ingredientLists;
        this.ingredients = ingredients;
        this.context = context;
        this.mViewModel = mViewModel;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.ingredient_item, parent, false);

        EditText ingredientName = convertView.findViewById(R.id.ingredient_name_edit_text);
        EditText ingredientQuantity = convertView.findViewById(R.id.ingredient_quantity_edit_text);

        setEditTextListeners(ingredientName, ingredientQuantity, position);
        renderAddButton(convertView, position);
        renderRemoveButton(convertView, position, ingredientName, ingredientQuantity);
        repopulateFields(ingredientName, ingredientQuantity, position);

        Spinner measurementSpinner = convertView.findViewById(R.id.measurement_spinner);
        List<String> measurementSpinnerOptions = Measurement.getAllMeasurementsStrings();
        ArrayAdapter spinnerAdapter = new ArrayAdapter(context,android.R.layout.simple_spinner_item,measurementSpinnerOptions);
        measurementSpinner.setAdapter(spinnerAdapter);

        //TODO Set up way to track using repopulate fields.
        Measurement currentItemMeasurement =mViewModel.getIngredientLists().get(position).getMeasurement();
        if(currentItemMeasurement!=Measurement.valueOf("NA") && currentItemMeasurement!=null){
            int spinnerPosition = spinnerAdapter.getPosition(currentItemMeasurement.name());
            measurementSpinner.setSelection(spinnerPosition);
        }

        measurementSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int positionSpinner, long id) {
                if(currentItemMeasurement == null || !measurementSpinnerOptions.get(positionSpinner).equals(currentItemMeasurement.name())) {
                    mViewModel.getIngredientLists().get(position).setMeasurement(Measurement.valueOf(measurementSpinnerOptions.get(positionSpinner)));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mViewModel.getIngredientLists().get(position).setMeasurement(Measurement.valueOf("NA"));
            }
        });

        return convertView;
    }

    //Used to update the data in the adapter
    public void setData(List<IngredientList> ingredientLists, List<Ingredient> ingredients){
        this.ingredientLists = ingredientLists;
        this.ingredients = ingredients;
        notifyDataSetChanged();
    }

    //Helper methods
    private void setEditTextListeners(EditText ingredientName, EditText ingredientQuantity, int position){
        ingredientName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.i("Ingredient Adapter" , " IngredientName: in beforeTextChanged");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.i("Ingredient Adapter" , "IngredientName: in beforeTextChanged");
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.i("Ingredient Adapter" , "IngredientName: in afterTextChanged");
                mViewModel.getIngredients().get(position).setName(s.toString());
            }
        });

        ingredientQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.i("Ingredient Adapter" , "IngredientQuantity: in beforeTextChanged");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.i("Ingredient Adapter" , "IngredientQuantity: in onTextChanged");
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.i("Ingredient Adapter" , "IngredientQuantity: in afterTextChanged");

                Pattern isNumberPattern = Pattern.compile("^\\d+(\\.\\d+)?$");
                Matcher matcher = isNumberPattern.matcher(s.toString());
                if(matcher.matches())
                {
                    mViewModel.getIngredientLists().get(position).setQuantity(Double.parseDouble(s.toString()));
                }
                else{
                    mViewModel.getIngredientLists().get(position).setQuantity(0);
                }
            }
        });
    }

    private void renderAddButton(View convertView, int position ){
        ConstraintLayout buttonLayout = convertView.findViewById(R.id.add_ingredient_constraint_layout);
        if(position!=ingredientLists.size()-1){
            buttonLayout.setVisibility(View.GONE);
        }
        else{
            //TODO see if we can only make changes to those elements who have updated
            if(((Activity)context).getCurrentFocus()!=null) {
                ((Activity)context).getCurrentFocus().clearFocus();
            }
            ImageButton addIngredientButton = convertView.findViewById(R.id.add_ingredient_button);
            addIngredientButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mViewModel.addIngredientLists();
                }
            });
        }
    }

    private void renderRemoveButton(View convertView, int position, EditText ingredientName, EditText ingredientQuantity) {
        ImageButton removeIngredientButton = convertView.findViewById(R.id.remove_ingredient_button);
        removeIngredientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position ==0 && ingredientLists.size() ==1){
                    ingredientName.setText("");
                    ingredientQuantity.setText("");
                }
                else{
                    mViewModel.removeIngredientList(position);
                }
            }
        });
    }

    private void repopulateFields(EditText ingredientName, EditText ingredientQuantity, int position){
        if(mViewModel.getIngredients().get(position).getName()!=null){
            ingredientName.setText(mViewModel.getIngredients().get(position).getName());
        }
        if(mViewModel.getIngredientLists().get(position).getQuantity() != 0){
            ingredientQuantity.setText(mViewModel.getIngredientLists().get(position).getQuantity() +"");
        }
    }
}