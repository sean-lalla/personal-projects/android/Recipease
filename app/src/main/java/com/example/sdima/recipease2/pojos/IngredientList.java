package com.example.sdima.recipease2.pojos;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;

import com.example.sdima.recipease2.enums.Measurement;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "ingredient_list",
        foreignKeys = {
            @ForeignKey(entity = Ingredient.class, parentColumns = "id", childColumns = "ingredient_id", onDelete = CASCADE),
                @ForeignKey(entity = Recipe.class, parentColumns = "id", childColumns = "recipe_id", onDelete = CASCADE)
        },
        primaryKeys = {"recipe_id","ingredient_id"}
        )
public class IngredientList {


    @ColumnInfo(name = "recipe_id")
    private long recipeId;

    @ColumnInfo(name = "ingredient_id")
    private long ingredientId;

    @ColumnInfo(name = "quantity")
    private double quantity;

    //Need to change this to an int
    @ColumnInfo(name= "measurement")
    private int measurementOrdinal;

    @Ignore
    private Measurement measurement;

    public IngredientList(){

    }

    public IngredientList(long recipeId, long ingredientId, double quantity, Measurement measurement){
        this.recipeId = recipeId;
        this.ingredientId = ingredientId;
        this.quantity = quantity;
        setMeasurement(measurement);
    }

    public IngredientList(long recipeId, long ingredientId, double quantity, int measurementOrdinal){
        this.recipeId = recipeId;
        this.ingredientId = ingredientId;
        this.quantity = quantity;
        setMeasurementOrdinal(measurementOrdinal);
    }

    public void setRecipeId(long recipeId) {
        this.recipeId = recipeId;
    }
    public long getRecipeId(){
        return recipeId;
    }

    public void setIngredientId(long ingredientId) {
        this.ingredientId = ingredientId;
    }
    public long getIngredientId(){
        return ingredientId;
    }

    public void setQuantity(double quantity){
        this.quantity = quantity;
    }
    public double getQuantity(){
        return quantity;
    }

    public void setMeasurement(Measurement measurement) {
        this.measurement = measurement;
        this.measurementOrdinal = measurement.ordinal();
    }
    public Measurement getMeasurement() {
        return measurement;
    }


    public void setMeasurementOrdinal(int measurementOrdinal){
        this.measurementOrdinal = measurementOrdinal;
        this.measurement = Measurement.getMeasurement(measurementOrdinal);
    }
    public int getMeasurementOrdinal(){
        return measurementOrdinal;
    }
}
