package com.example.sdima.recipease2.ui.viewrecipes;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.sdima.recipease2.database.RecipeaseDatabaseInstance;
import com.example.sdima.recipease2.pojos.Recipe;

import java.util.ArrayList;
import java.util.List;

//This extends Android View Model since we will need the context in order to do db tasks.
public class ViewRecipesViewModel extends AndroidViewModel {
    private ArrayList<Recipe> recipes;
    private LiveData<List<Recipe>> recipesCollectionLive;
    MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
    RecipeaseDatabaseInstance instance;


    private Context context;

    public ViewRecipesViewModel(@NonNull Application application) {
        super(application);
        recipes = new ArrayList<Recipe>();
        context = application.getApplicationContext();
        recipesCollectionLive = new MutableLiveData<>();
        instance = new RecipeaseDatabaseInstance(getApplication());
        updateRecipesList();
    }

    public LiveData<List<Recipe>> getRecipesCollectionLive(){
        return recipesCollectionLive;
    }

    public ArrayList<Recipe> getRecipes() {
        return recipes;
    }

    //TODO This call likely should only return the name and id of each recipe to make the call more optimized. This will prevent un-needed data from being stored before being needed
    public void updateRecipesList() {
        recipesCollectionLive = instance.getAllRecipes();
    }

    public void setText(String s){
        mutableLiveData.setValue(s);
    }

    public MutableLiveData<String> getText(){
        return mutableLiveData;
    }

    public void deleteRecipes(List<Recipe> recipes){
        for(Recipe recipe: recipes){
            instance.deleteRecipe(recipe);
        }
    }
}