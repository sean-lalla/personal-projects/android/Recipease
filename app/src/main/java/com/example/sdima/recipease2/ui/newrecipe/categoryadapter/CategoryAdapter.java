package com.example.sdima.recipease2.ui.newrecipe.categoryadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.sdima.recipease2.R;
import com.example.sdima.recipease2.ui.newrecipe.NewRecipeFragmentViewModel;

import java.util.ArrayList;
import java.util.List;

public class CategoryAdapter extends ArrayAdapter<CategoryState> {
    private Context mContext;
    private ArrayList<CategoryState> listState;
    private CategoryAdapter myAdapter;
    private boolean isFromView = false;
    private NewRecipeFragmentViewModel mViewModel;

    public CategoryAdapter(Context context, int resource, List<CategoryState> objects, NewRecipeFragmentViewModel mViewModel) {
        super(context, resource, objects);
        this.mContext = context;
        this.listState = (ArrayList<CategoryState>) objects;
        this.myAdapter = this;
        this.mViewModel = mViewModel;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView,
                              ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.category_spinner_item, null);
            holder = new ViewHolder();
            holder.mTextView = convertView
                    .findViewById(R.id.category_text);
            holder.mCheckBox = convertView
                    .findViewById(R.id.category_checkbox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextView.setText(listState.get(position).getTitle());

        // Checks whether checked event fired from getView() or user input
        isFromView = true;
        holder.mCheckBox.setChecked(listState.get(position).isSelected());
        isFromView = false;

        if ((position == 0)) {
            holder.mCheckBox.setVisibility(View.INVISIBLE);
        } else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
        }
        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int getPosition = (Integer) buttonView.getTag();

                if (!isFromView) {
                    listState.get(position).setSelected(isChecked);
                    mViewModel.setCategories(listState);
                }
            }
        });
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }
}