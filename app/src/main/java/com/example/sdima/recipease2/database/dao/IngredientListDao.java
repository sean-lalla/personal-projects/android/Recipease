package com.example.sdima.recipease2.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.sdima.recipease2.pojos.IngredientList;

import java.util.List;

@Dao
public interface IngredientListDao {

    //Inserts
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertIngredientList(IngredientList ingredientList);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertIngredientLists(IngredientList[] ingredientLists);

    @Update
    int updateIngredientList(IngredientList ingredientList);

    //Deletes
    @Delete
    void deleteIngredientList(IngredientList ingredientList);

    @Delete
    void deleteIngredientLists(IngredientList[] ingredientLists);

    @Query("DELETE FROM ingredient_list")
    void deleteAll();

    @Query("DELETE FROM ingredient_list WHERE recipe_id = :id")
    void deleteIngredientListByRecipeId(long id);

    //Queries
    @Query("SELECT * FROM ingredient_list IL JOIN recipe R ON IL.recipe_id = R.id WHERE IL.recipe_id = :id")
    LiveData<List<IngredientList>> getIngredientListByRecipeId(long id);


}
