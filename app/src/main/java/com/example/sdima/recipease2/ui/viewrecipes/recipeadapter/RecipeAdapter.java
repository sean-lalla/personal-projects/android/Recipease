package com.example.sdima.recipease2.ui.viewrecipes.recipeadapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sdima.recipease2.R;
import com.example.sdima.recipease2.pojos.Recipe;
import com.example.sdima.recipease2.ui.viewrecipes.ViewRecipesViewModel;

import java.util.ArrayList;
import java.util.List;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.MyViewHolder> {
    Context context;
    List<Recipe> recipes;
    List<Recipe> selectedRecipes;
    List<ImageView> checkBoxes;
    List<CardView> cardViews;
    boolean isSelecting = false;
    ViewRecipesViewModel viewRecipesViewModel;


    public RecipeAdapter(Context context, List<Recipe> recipes, ViewRecipesViewModel mViewModel) {
        this.context = context;
        this.recipes = recipes;
        selectedRecipes = new ArrayList<Recipe>();
        checkBoxes = new ArrayList<ImageView>();
        cardViews = new ArrayList<CardView>();
        viewRecipesViewModel = mViewModel;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.recipe_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.recipeName.setText(recipes.get(holder.getAdapterPosition()).getName());
        holder.recipeDescription.setText(recipes.get(holder.getAdapterPosition()).getDescription());

        setListeners(holder);
    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
        notifyDataSetChanged();
    }

    private void setListeners(MyViewHolder holder){
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(!isSelecting){
                    ActionMode.Callback callback = new ActionMode.Callback() {
                        @Override
                        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                            MenuInflater menuInflater = mode.getMenuInflater();

                            menuInflater.inflate(R.menu.select_menu, menu);
                            return true;
                        }

                        @Override
                        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                            isSelecting = true;

                            clickItem(holder);
                            viewRecipesViewModel.getText().observe((LifecycleOwner)context, new Observer<String>() {
                                @Override
                                public void onChanged(String s) {
                                    mode.setTitle(String.format("%s Selected", s));
                                }
                            });
                            return true;
                        }

                        @Override
                        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                            int id = item.getItemId();
                            switch (id){
                                case R.id.menu_delete:
                                    confirmDelete(mode);
                                    break;
                            }
                            return true;
                        }

                        @Override
                        public void onDestroyActionMode(ActionMode mode) {
                            isSelecting = false;
                            selectedRecipes.clear();
                            clearPositions();
                            cardViews.clear();
                            checkBoxes.clear();
                        }
                    };
                    ((Toolbar)((AppCompatActivity) context).findViewById(R.id.toolbar)).startActionMode(callback);
                }
                else{
                    clickItem(holder);
                }
                return true;
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSelecting) {
                    clickItem(holder);
                }
                else {
                    Toast.makeText(context, "You have clicked the element at " + holder.getAdapterPosition(),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void clickItem(MyViewHolder holder) {
        Recipe recipe = recipes.get(holder.getAdapterPosition());

        if (holder.checkbox.getVisibility() == View.INVISIBLE) {
            holder.checkbox.setVisibility(View.VISIBLE);
            holder.recipeRow.setBackgroundColor(Color.LTGRAY);
            selectedRecipes.add(recipe);
            checkBoxes.add(holder.checkbox);
            cardViews.add(holder.recipeRow);
        }
        else{
            holder.checkbox.setVisibility(View.INVISIBLE);
            holder.recipeRow.setBackgroundColor(Color.WHITE);
            selectedRecipes.remove(recipe);
            checkBoxes.remove(holder.checkbox);
            cardViews.remove(holder.recipeRow);
        }
        viewRecipesViewModel.setText(""+selectedRecipes.size());
    }

    private void clearPositions(){
        for (ImageView checkBox: checkBoxes){
            checkBox.setVisibility(View.INVISIBLE);
        }
        for (CardView cardView: cardViews){
            cardView.setBackgroundColor(Color.WHITE);
        }
    }

    private void confirmDelete(ActionMode mode){
        AlertDialog.Builder builder = new AlertDialog.Builder((AppCompatActivity)context);
        String message = (selectedRecipes.size() > 1) ? "Are you sure you would like to delete these " + selectedRecipes.size() + " items?" : "Are you sure you'd like to delete " + selectedRecipes.get(0).getName() +"?";

        builder.setMessage(message)
                .setTitle("Delete recipes?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        viewRecipesViewModel.deleteRecipes(selectedRecipes);
                        mode.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        builder.show();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView recipeName, recipeDescription;
        ImageView recipeImage, checkbox;
        CardView recipeRow;

        public MyViewHolder(@NonNull View itemView){
            super(itemView);
            recipeName = itemView.findViewById(R.id.recipe_name);
            recipeDescription = itemView.findViewById(R.id.recipe_description);
            recipeImage = itemView.findViewById(R.id.recipe_image);
            recipeRow = itemView.findViewById(R.id.recipe_card);
            checkbox = itemView.findViewById(R.id.checkbox);
        }
    }
}
