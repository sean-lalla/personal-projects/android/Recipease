package com.example.sdima.recipease2.ui.splash;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;

import com.example.sdima.recipease2.DrawerLocker;
import com.example.sdima.recipease2.R;

public class SplashFragment extends Fragment {

    private SplashViewModel splashViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        splashViewModel =
                ViewModelProviders.of(this).get(SplashViewModel.class);
        View root = inflater.inflate(R.layout.fragment_splash_screen, container, false);
        Runnable moveToLogin = new Runnable() {
            @Override
            public void run() {
                NavHostFragment.findNavController(SplashFragment.this)
                        .navigate(R.id.action_nav_home_to_nav_recipes_fragment);
            }
        };
        root.postDelayed(moveToLogin, 3500);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        ((DrawerLocker)getActivity()).setDrawerEnabled(false);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        ((DrawerLocker)getActivity()).setDrawerEnabled(true);
        super.onDestroy();
    }
}
