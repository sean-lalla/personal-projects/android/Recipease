package com.example.sdima.recipease2.pojos;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.example.sdima.recipease2.enums.Category;

import java.util.ArrayList;

@Entity(tableName = "recipe")
public class Recipe {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "description")
    private String description;

    //TODO new table
    @Ignore
    private ArrayList<Category> recipeCategories;

    @ColumnInfo(name = "time")
    private int time;

    public Recipe(){
    }
    public Recipe(int id, String name, String description, ArrayList<Category> recipeCategories, int time){
        this.id = id;
        this.name = name;
        this.description = description;
        this.recipeCategories = recipeCategories;
        this.time = time;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName(){
        return name;
    }

    public void setId(long id){
        this.id = id;
    }
    public long getId(){
        return id;
    }

    public void setDescription(String description){
        this.description = description;
    }
    public String getDescription(){
        return description;
    }

    public void setTime(int time){
        this.time = time;
    }
    public int getTime(){
        return time;
    }

    public ArrayList<Category> getCategories(){
        return recipeCategories;
    }
    public void setCategories(ArrayList<Category> recipeCategories){
        this.recipeCategories = recipeCategories;
    }
    public String writeCategories(){
        String categories ="";
        if(recipeCategories!=null) {
            for (Category c : recipeCategories) {
                categories += c.ordinal() + " ";
            }
        }
        return categories;
    }
}
