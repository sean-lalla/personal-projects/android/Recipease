package com.example.sdima.recipease2.ui.newrecipe;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.sdima.recipease2.database.RecipeaseDatabaseInstance;
import com.example.sdima.recipease2.enums.Category;
import com.example.sdima.recipease2.pojos.Ingredient;
import com.example.sdima.recipease2.pojos.IngredientList;
import com.example.sdima.recipease2.pojos.Recipe;
import com.example.sdima.recipease2.pojos.RecipeCategories;
import com.example.sdima.recipease2.pojos.RecipeDirections;
import com.example.sdima.recipease2.ui.newrecipe.categoryadapter.CategoryState;

import java.util.ArrayList;
import java.util.List;

public class NewRecipeFragmentViewModel extends AndroidViewModel {
    private List<IngredientList> ingredientLists;
    private List<Ingredient> ingredients;
    private MutableLiveData<List<IngredientList>> ingredientListsLive;
    private MutableLiveData<List<Ingredient>> ingredientsLive;
    private List<RecipeDirections> recipeDirections;
    private MutableLiveData<List<RecipeDirections>> recipeDirectionsLive;
    private List<RecipeCategories> recipeCategories;
    private MutableLiveData<List<RecipeCategories>> recipeCategoriesLive;
    private Recipe recipe;
    private RecipeaseDatabaseInstance recipeaseDatabaseInstance;

    private Context context;

    public NewRecipeFragmentViewModel(@NonNull Application application) {
        super(application);
        recipeaseDatabaseInstance = new RecipeaseDatabaseInstance(application);
        ingredientListsLive = new MutableLiveData<>();
        ingredientsLive = new MutableLiveData<>();
        recipeDirectionsLive = new MutableLiveData<>();
        recipeCategoriesLive = new MutableLiveData<>();
        recipe = new Recipe();
        addRecipeDirections();
        addIngredientLists();
        context = application.getApplicationContext();
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public List<RecipeDirections> getRecipeDirections() {
        return recipeDirections;
    }

    public MutableLiveData<List<RecipeDirections>> getRecipeDirectionsLive() {
        return recipeDirectionsLive;
    }

    public void addRecipeDirections(){
        if (recipeDirections == null) {
            recipeDirections = new ArrayList<RecipeDirections>();
        }
        recipeDirections.add(new RecipeDirections());

        recipeDirectionsLive.postValue(recipeDirections);

        Log.i("New Recipe Fragment View Model: ", "Recipe directions have been added");
    }

    public void removeRecipeDirections(int index){
        recipeDirections.remove(index);
        recipeDirectionsLive.postValue(recipeDirections);
        Log.i("New Recipe Fragment View Model: ", "RecipeDirections at position "+index + " was removed.");
    }

    public MutableLiveData<List<IngredientList>> getIngredientListsLive() {
        return ingredientListsLive;
    }

    public MutableLiveData<List<Ingredient>> getIngredientsLive() {
        return ingredientsLive;
    }

    public List<IngredientList> getIngredientLists() {
        return ingredientLists;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void addIngredientLists() {
        if (ingredientLists == null && ingredients == null) {
            ingredientLists = new ArrayList<IngredientList>();
            ingredients = new ArrayList<Ingredient>();
            ingredients.add(new Ingredient());
            ingredientLists.add(new IngredientList());
        } else {
            ingredients.add(new Ingredient());
            ingredientLists.add(new IngredientList());
        }

        ingredientListsLive.postValue(ingredientLists);
        ingredientsLive.postValue(ingredients);

        Log.i("New Recipe Fragment View Model: ",  "Items have been added");
    }

    public void removeIngredientList(int index){
        ingredients.remove(index);
        ingredientLists.remove(index);
        ingredientListsLive.postValue(ingredientLists);
        ingredientsLive.postValue(ingredients);
        Log.i("New Recipe Fragment View Model: ", "Item at position "+index + " was removed.");
    }

    public void removeLastIngredientList() {
        if (ingredientLists.size() > 1 || ingredients.size() > 1){
            ingredients.remove(ingredients.size()-1);
            ingredientLists.remove(ingredients.size()-1);
            ingredientListsLive.postValue(ingredientLists);
            ingredientsLive.postValue(ingredients);
            Log.i("New Recipe Fragment View Model: ", "Items have removed");
        }
    }

    public void setCategories(ArrayList<CategoryState> listState) {
        ArrayList<RecipeCategories> recipeCategoriesList = new ArrayList<RecipeCategories>();
        for(int i =1; i< listState.size(); i++){
            if(listState.get(i).isSelected()){
                RecipeCategories recipeCategories = new RecipeCategories();
                recipeCategories.setCategory(Category.valueOf(listState.get(i).getTitle()));
                recipeCategoriesList.add(recipeCategories);
            }
        }
        recipeCategoriesLive.setValue(recipeCategoriesList);
        recipeCategories = recipeCategoriesList;
        Log.i("NewRecipeFragmentViewModel", recipeCategoriesList.toString());
    }

    public void saveRecipe() {
        long id = recipeaseDatabaseInstance.insertRecipe(recipe);
        for (int i =0; i<ingredients.size(); i++) {
            long ingredientId = recipeaseDatabaseInstance.insertIngredient(ingredients.get(i));
            ingredientLists.get(i).setRecipeId(id);
            ingredientLists.get(i).setIngredientId(ingredientId);
            recipeaseDatabaseInstance.insertIngredientList(ingredientLists.get(i));
        }

        if (recipeDirections != null){
            for (RecipeDirections rd : recipeDirections) {
                rd.setRecipeID(id);
                recipeaseDatabaseInstance.insertRecipeDirection(rd);
            }
        }

        if(recipeCategories != null) {
            for (RecipeCategories rc : recipeCategories) {
                rc.setRecipeId(id);
                recipeaseDatabaseInstance.insertRecipeCategories(rc);
            }
        }
    }
}