package com.example.sdima.recipease2.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.sdima.recipease2.pojos.RecipeDirections;

import java.util.List;

@Dao
public interface RecipeDirectionsDao {

    //Inserts
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertRecipeDirection(RecipeDirections recipeDirection);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertRecipeDirections(RecipeDirections[] recipeDirections);

    //Delete
    @Delete
    void deleteRecipeDirection(RecipeDirections recipeDirection);

    @Query("DELETE FROM recipe_directions")
    void deleteAllRecipeDirections();

    @Query("DELETE FROM recipe_directions WHERE recipe_id = :id")
    void deleteRecipeDirectionsByRecipeId(long id);

    //Update
    @Update
    int updateRecipeDirection(RecipeDirections directions);

    //Queries
    @Query("SELECT * FROM recipe_directions RD WHERE RD.recipe_id = :id")
    LiveData<List<RecipeDirections>> getRecipeDirectionsByRecipeId(long id);

}
