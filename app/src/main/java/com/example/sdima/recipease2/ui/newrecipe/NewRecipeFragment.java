package com.example.sdima.recipease2.ui.newrecipe;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.sdima.recipease2.R;
import com.example.sdima.recipease2.enums.Category;
import com.example.sdima.recipease2.pojos.Ingredient;
import com.example.sdima.recipease2.pojos.IngredientList;
import com.example.sdima.recipease2.pojos.RecipeDirections;
import com.example.sdima.recipease2.ui.components.NoScrollListView;
import com.example.sdima.recipease2.ui.newrecipe.categoryadapter.CategoryAdapter;
import com.example.sdima.recipease2.ui.newrecipe.categoryadapter.CategoryState;
import com.example.sdima.recipease2.ui.newrecipe.ingredientadapter.IngredientAdapter;
import com.example.sdima.recipease2.ui.newrecipe.recipedirectionadapter.RecipeDirectionAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class NewRecipeFragment extends Fragment {

    private NewRecipeFragmentViewModel mViewModel;

    public static NewRecipeFragment newInstance() {
        return new NewRecipeFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(NewRecipeFragmentViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.new_recipe_fragment, container, false);
        IngredientAdapter ingredientAdapter = new IngredientAdapter(getActivity(),
                mViewModel.getIngredientLists(), mViewModel.getIngredients(), mViewModel);
        NoScrollListView ingredientsNoScrollView = v.findViewById(R.id.recipe_ingredients_no_scroll_view);
        ingredientsNoScrollView.setAdapter(ingredientAdapter);
        mViewModel.getIngredientListsLive().observe(getViewLifecycleOwner(),
                ingredientLists -> setNoScrollIngredientAdapter(ingredientAdapter, ingredientLists, mViewModel.getIngredients()));

        RecipeDirectionAdapter recipeDirectionAdapter = new RecipeDirectionAdapter(getActivity(), mViewModel.getRecipeDirections(), mViewModel);
        NoScrollListView recipeDirectionsNoScrollView = v.findViewById(R.id.recipe_steps_no_scroll_list_view);
        recipeDirectionsNoScrollView.setAdapter(recipeDirectionAdapter);
        mViewModel.getRecipeDirectionsLive().observe(getViewLifecycleOwner(),
                recipeDirections -> setNoScrollRecipeDirectionsAdapter(recipeDirectionAdapter, recipeDirections));

        EditText recipeTimeText = v.findViewById(R.id.recipe_time_text);
        recipeTimeText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    String timeText = recipeTimeText.getText().toString();
                    if(!(timeText).equals("")){
                        int recipeTime = Integer.parseInt(timeText);
                        if(mViewModel.getRecipe().getTime()!= recipeTime){
                            mViewModel.getRecipe().setTime(recipeTime);
                        }
                    }
                }
            }
        });

        EditText recipeDescriptionText = v.findViewById(R.id.recipe_description_text);
        recipeDescriptionText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    String recipeDescription = recipeDescriptionText.getText().toString();
                    if(!(recipeDescription).equals("")){
                        if(!recipeDescription.equals(mViewModel.getRecipe().getDescription())){
                            mViewModel.getRecipe().setDescription(recipeDescription);
                        }
                    }
                }
            }
        });

        EditText recipeNameText = v.findViewById(R.id.recipe_name_text);
        recipeNameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    String recipeName = recipeNameText.getText().toString();
                    if(!(recipeName).equals("")){
                        if(!recipeName.equals(mViewModel.getRecipe().getName())){
                            mViewModel.getRecipe().setName(recipeName);
                        }
                    }
                }
            }
        });

        Spinner categoriesSpinner = v.findViewById(R.id.recipe_categories_spinner);
        List<String> selectCategory = Category.getAllCategoryStrings();
        ArrayList<CategoryState> categoryStates = new ArrayList<>();
        CategoryState prompt = new CategoryState();
        prompt.setTitle("Categories");
        prompt.setSelected(false);
        categoryStates.add(prompt);

        for (int i = 0; i < selectCategory.size(); i++) {
            CategoryState categoryState = new CategoryState();
            categoryState.setTitle(selectCategory.get(i));
            categoryState.setSelected(false);
            categoryStates.add(categoryState);
        }
        CategoryAdapter myAdapter = new CategoryAdapter(getContext(), 0,
                categoryStates, mViewModel);
        categoriesSpinner.setAdapter(myAdapter);

        FloatingActionButton saveButton = v.findViewById(R.id.saveRecipeButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View focus = getActivity().getCurrentFocus();
                if (focus!=null){
                    focus.clearFocus();
                }
                mViewModel.saveRecipe();
                NavController navController = Navigation.findNavController(v);
                navController.navigate(R.id.action_nav_new_recipe_fragment_to_nav_recipes_fragment);
            }
        });

        return v;
    }

    public void setNoScrollIngredientAdapter(IngredientAdapter adapter, List<IngredientList> ingredientLists, List<Ingredient> ingredients){
        adapter.setData(ingredientLists, ingredients);
    }

    public void setNoScrollRecipeDirectionsAdapter(RecipeDirectionAdapter adapter, List<RecipeDirections> recipeDirections){
        adapter.setData(recipeDirections);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


}