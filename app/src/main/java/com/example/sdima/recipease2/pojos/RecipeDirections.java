package com.example.sdima.recipease2.pojos;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "recipe_directions",
        foreignKeys = {
            @ForeignKey(entity = Recipe.class, parentColumns = "id", childColumns = "recipe_id", onDelete = CASCADE),
        },
        primaryKeys = {"recipe_id","recipe_step"})
public class RecipeDirections {

    @ColumnInfo(name = "recipe_id")
    private long recipeID;

    @ColumnInfo(name = "recipe_step")
    private int recipeStep;

    @ColumnInfo(name = "direction")
    private String stepDirections;

    public RecipeDirections(){
    }
    public RecipeDirections(long recipeID, int recipeStep, String stepDirections){
        this.recipeID = recipeID;
        this.recipeStep = recipeStep;
        this.stepDirections = stepDirections;
    }

    public void setRecipeID(long recipeID){
        this.recipeID = recipeID;
    }
    public long getRecipeID(){
        return recipeID;
    }

    public void setRecipeStep(int recipeStep) {
        this.recipeStep = recipeStep;
    }
    public int getRecipeStep() {
        return recipeStep;
    }

    public void setStepDirections(String stepDirections) {
        this.stepDirections = stepDirections;
    }
    public String getStepDirections() {
        return stepDirections;
    }
}
