package com.example.sdima.recipease2.database;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.sdima.recipease2.database.dao.IngredientDao;
import com.example.sdima.recipease2.database.dao.IngredientListDao;
import com.example.sdima.recipease2.database.dao.RecipeCategoriesDao;
import com.example.sdima.recipease2.database.dao.RecipeDao;
import com.example.sdima.recipease2.database.dao.RecipeDirectionsDao;
import com.example.sdima.recipease2.pojos.Ingredient;
import com.example.sdima.recipease2.pojos.IngredientList;
import com.example.sdima.recipease2.pojos.Recipe;
import com.example.sdima.recipease2.pojos.RecipeCategories;
import com.example.sdima.recipease2.pojos.RecipeDirections;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;


public class RecipeaseDatabaseInstance {

    private IngredientDao mIngredientDao;
    private IngredientListDao mIngredientListDao;
    private RecipeDao mRecipeDao;
    private RecipeDirectionsDao mRecipeDirectionsDao;
    private RecipeCategoriesDao mRecipeCategoriesDao;

    // Note that in order to unit test the RecipeaseDatabaseInstance, you have to remove the Application
    // dependency. This adds complexity and much more code, and this sample is not about testing.
    // See the BasicSample in the android-architecture-components repository at
    // https://github.com/googlesamples
    public RecipeaseDatabaseInstance(Application application) {
        RecipeaseDatabase db = RecipeaseDatabase.getDatabase(application);
        mIngredientDao = db.ingredientDao();
        mIngredientListDao = db.ingredientListDao();
        mRecipeDao = db.recipeDao();
        mRecipeDirectionsDao = db.recipeDirectionsDao();
        mRecipeCategoriesDao = db.recipeCategoriesDao();
    }

    //Ingredients
    public long insertIngredient(Ingredient ingredient){
        Callable<Long> insertCallable = () -> mIngredientDao.insertIngredient(ingredient);
        long ingredientID = 0;

        Future<Long> future = RecipeaseDatabase.databaseWriteExecutor.submit(insertCallable);
        try {
            ingredientID = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return ingredientID;
    }

    public List<Long> insertIngredients(Ingredient[] ingredients){
        Callable<List<Long>> insertCallable = () -> mIngredientDao.insertIngredients(ingredients);
        List<Long> ingredientIDs = new ArrayList<Long>();

        Future<List<Long>> future = RecipeaseDatabase.databaseWriteExecutor.submit(insertCallable);
        try {
            ingredientIDs = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return ingredientIDs;
    }

    public void deleteIngredient(Ingredient ingredient){
        RecipeaseDatabase.databaseWriteExecutor.execute(() -> {
            mIngredientDao.deleteIngredient(ingredient);
        });
    }

    public void deleteIngredientById(long id){
        RecipeaseDatabase.databaseWriteExecutor.execute(() -> {
            mIngredientDao.deleteIngredientById(id);
        });
    }

    public void deleteAllIngredients(){
        RecipeaseDatabase.databaseWriteExecutor.execute(() -> {
            mIngredientDao.deleteAllIngredients();
        });
    }

    public int updateIngredient(Ingredient ingredient){
        Callable<Integer> updateCallable = () -> mIngredientDao.updateIngredient(ingredient);
        int updated = 0;

        Future<Integer> future = RecipeaseDatabase.databaseWriteExecutor.submit(updateCallable);
        try {
            updated = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return updated;
    }

    public LiveData<List<Ingredient>> getAlphabetizedIngredients(){
        return mIngredientDao.getAlphabetizedIngredients();
    }

    public LiveData<List<Ingredient>> getAllIngredients(){
        return mIngredientDao.getAllIngredients();
    }

    public LiveData<Ingredient> getIngredientByID(long id){
        return mIngredientDao.getIngredientByID(id);
    }

    public LiveData<Ingredient> getIngredientByName(String name){
        return mIngredientDao.getIngredientByName(name);
    }

    public LiveData<List<Ingredient>> getIngredientsWithNamesLike(String search){
        return mIngredientDao.getIngredientsWithNamesLike(search);
    }

    public LiveData<List<Ingredient>> getIngredientsStartingWith(String search){
        return mIngredientDao.getIngredientsStartingWith(search);
    }

    //IngredientList
    public void insertIngredientList(IngredientList ingredientList){
        RecipeaseDatabase.databaseWriteExecutor.execute(() -> {
            mIngredientListDao.insertIngredientList(ingredientList);
        });
    }

    public void insertIngredientLists(IngredientList[] ingredientLists){
        RecipeaseDatabase.databaseWriteExecutor.execute(() -> {
            mIngredientListDao.insertIngredientLists(ingredientLists);
        });
    }

    public int updateIngredientList(IngredientList ingredientList){
        Callable<Integer> updateCallable = () -> mIngredientListDao.updateIngredientList(ingredientList);
        int updated = 0;

        Future<Integer> future = RecipeaseDatabase.databaseWriteExecutor.submit(updateCallable);
        try {
            updated = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return updated;
    }

    public void deleteIngredientList(IngredientList ingredientList) {
        RecipeaseDatabase.databaseWriteExecutor.execute(() ->{
            mIngredientListDao.deleteIngredientList(ingredientList);
        });
    }

    public void deleteIngredientLists(IngredientList[] ingredientLists){
        RecipeaseDatabase.databaseWriteExecutor.execute(()->{
            mIngredientListDao.deleteIngredientLists(ingredientLists);
        });
    }

    public void deleteAllIngredientLists(){
        RecipeaseDatabase.databaseWriteExecutor.execute(()->{
            mIngredientListDao.deleteAll();
        });
    }

    public void deleteIngredientListByRecipeId(long id){
        RecipeaseDatabase.databaseWriteExecutor.execute(()->{
            mIngredientListDao.deleteIngredientListByRecipeId(id);
        });
    }

    public LiveData<List<IngredientList>> getIngredientListByRecipeId(long id){
        return mIngredientListDao.getIngredientListByRecipeId(id);
    }

    //Recipe
    public void insertRecipeCategories(RecipeCategories recipeCategories){
        RecipeaseDatabase.databaseWriteExecutor.execute(()->{
            mRecipeCategoriesDao.insertRecipeCategories(recipeCategories);
        });
    }

    public void insertRecipeCategoriesList(RecipeCategories[] recipeCategoriesList){
        RecipeaseDatabase.databaseWriteExecutor.execute(()->{
            mRecipeCategoriesDao.insertRecipeCategories(recipeCategoriesList);
        });
    }

    public void deleteRecipeCategories(RecipeCategories recipeCategories){
        RecipeaseDatabase.databaseWriteExecutor.execute(()->{
            mRecipeCategoriesDao.deleteRecipeCategories(recipeCategories);
        });
    }

    public void deleteAllRecipeCategories(){
        RecipeaseDatabase.databaseWriteExecutor.execute(()->{
            mRecipeCategoriesDao.deleteAllRecipeCategories();
        });
    }

    public void deleteAllRecipeCategoriesByRecipeId(long id){
        RecipeaseDatabase.databaseWriteExecutor.execute(()->{
            mRecipeCategoriesDao.deleteAllRecipeCategoriesByRecipeId(id);
        });
    }

    public LiveData<List<RecipeCategories>> getRecipeCategoriesByRecipeId(long id){
        return mRecipeCategoriesDao.getRecipeCategoriesByRecipeId(id);
    }

    //Recipes
    public long insertRecipe(Recipe recipe){
        Callable<Long> insertCallable = () -> mRecipeDao.insertRecipe(recipe);
        long recipeID = 0;

        Future<Long> future = RecipeaseDatabase.databaseWriteExecutor.submit(insertCallable);
        try {
            recipeID = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return recipeID;
    }

    public List<Long> insertRecipes(Recipe[] recipes){
        Callable<List<Long>> insertCallable = () -> mRecipeDao.insertRecipes(recipes);
        List<Long> recipeIDs = new ArrayList<Long>();

        Future<List<Long>> future = RecipeaseDatabase.databaseWriteExecutor.submit(insertCallable);
        try {
            recipeIDs = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return recipeIDs;
    }

    public int updateRecipe(Recipe recipe){
        Callable<Integer> insertCallable = () -> mRecipeDao.updateRecipe(recipe);
        int updated = 0;

        Future<Integer> future = RecipeaseDatabase.databaseWriteExecutor.submit(insertCallable);
        try {
            updated = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return updated;
    }

    public void deleteRecipe(Recipe recipe){
        RecipeaseDatabase.databaseWriteExecutor.execute(()->{
            mRecipeDao.deleteRecipe(recipe);
        });
    }

    public void deleteRecipes(Recipe[] recipes){
        RecipeaseDatabase.databaseWriteExecutor.execute(()->{
            mRecipeDao.deleteRecipes(recipes);
        });
    }

    public void deleteAllRecipes(){
        RecipeaseDatabase.databaseWriteExecutor.execute(()->{
            mRecipeDao.deleteAll();
        });
    }

    public LiveData<Recipe> getRecipeById(long id){
        return mRecipeDao.getRecipeById(id);
    }

    public LiveData<List<Recipe>> getRecipesById(long[] ids){
        return mRecipeDao.getRecipesById(ids);
    }

     public LiveData<List<Recipe>> getAllRecipes(){
        return mRecipeDao.getAllRecipes();
     }

     public LiveData<List<Recipe>> getRecipesContainingName(String search){
        return mRecipeDao.getRecipesContainingName(search);
     }

     public LiveData<List<Recipe>> getRecipesStartingWithName(String search){
        return mRecipeDao.getRecipesStartingWithName(search);
     }

     public LiveData<List<Long>> getRecipesUsingIngredient(long id){
        return mRecipeDao.getRecipesUsingIngredient(id);
     }

     //RecipeDirections
    public void insertRecipeDirection(RecipeDirections recipeDirection){
        RecipeaseDatabase.databaseWriteExecutor.execute(() -> {
            mRecipeDirectionsDao.insertRecipeDirection(recipeDirection);
        });
    }

   public void insertRecipeDirections(RecipeDirections[] recipeDirections){
       RecipeaseDatabase.databaseWriteExecutor.execute(() -> {
           mRecipeDirectionsDao.insertRecipeDirections(recipeDirections);
       });
   }

    public void deleteRecipeDirection(RecipeDirections recipeDirections){
        RecipeaseDatabase.databaseWriteExecutor.execute(() -> {
            mRecipeDirectionsDao.deleteRecipeDirection(recipeDirections);
        });
    }

    public void deleteAllRecipeDirections(){
        RecipeaseDatabase.databaseWriteExecutor.execute(() -> {
            mRecipeDirectionsDao.deleteAllRecipeDirections();
        });
    }

    public void deleteRecipeDirectionsByRecipeId(long id){
        RecipeaseDatabase.databaseWriteExecutor.execute(() -> {
            mRecipeDirectionsDao.deleteRecipeDirectionsByRecipeId(id);
        });
    }


    public int updateRecipeDirection(RecipeDirections directions){
        Callable<Integer> updateCallable = () -> mRecipeDirectionsDao.updateRecipeDirection(directions);
        int updated = 0;

        Future<Integer> future = RecipeaseDatabase.databaseWriteExecutor.submit(updateCallable);
        try {
            updated = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return updated;
    }

    public LiveData<List<RecipeDirections>> getRecipeDirectionsByRecipeId(long id){
        return mRecipeDirectionsDao.getRecipeDirectionsByRecipeId(id);
    }
}
