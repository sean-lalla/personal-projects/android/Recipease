package com.example.sdima.recipease2;
import android.content.Context;


import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.sdima.recipease2.database.RecipeaseDatabase;
import com.example.sdima.recipease2.database.dao.IngredientDao;
import com.example.sdima.recipease2.database.dao.IngredientListDao;
import com.example.sdima.recipease2.database.dao.RecipeCategoriesDao;
import com.example.sdima.recipease2.database.dao.RecipeDao;
import com.example.sdima.recipease2.database.dao.RecipeDirectionsDao;
import com.example.sdima.recipease2.enums.Category;
import com.example.sdima.recipease2.pojos.Ingredient;
import com.example.sdima.recipease2.pojos.IngredientList;
import com.example.sdima.recipease2.enums.Measurement;
import com.example.sdima.recipease2.pojos.Recipe;
import com.example.sdima.recipease2.pojos.RecipeCategories;
import com.example.sdima.recipease2.pojos.RecipeDirections;


import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * This class tests the database.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class DatabaseTest {
    private IngredientDao ingredientDao;
    private RecipeDao recipeDao;
    private IngredientListDao ingredientListDao;
    private RecipeCategoriesDao recipeCategoriesDao;
    private RecipeDirectionsDao recipeDirectionsDao;
    private RecipeaseDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, RecipeaseDatabase.class).allowMainThreadQueries().build();
        ingredientDao = db.ingredientDao();
        recipeDao = db.recipeDao();
        ingredientListDao = db.ingredientListDao();
        recipeCategoriesDao = db.recipeCategoriesDao();
        recipeDirectionsDao = db.recipeDirectionsDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    //Required to allow running on background thread.
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Test
    public void manageIngredients() throws Exception {
        String ingredientName = "Kimchi";
		Ingredient ingredient = new Ingredient();
		ingredient.setName(ingredientName);

		//Insert Ingredient
		long id = ingredientDao.insertIngredient(ingredient);
		assertNotEquals(-1, id);

		//Insert Ingredients
        Ingredient[] ingredients = new Ingredient[2];
        ingredients[0] = new Ingredient();
        ingredients[0].setName("Gochujang");
        ingredients[1] = new Ingredient();
        ingredients[1].setName("Gochugaru");

        List<Long> ids = ingredientDao.insertIngredients(ingredients);
        assertEquals(2, ids.size());

		//Get IngredientByID
		Ingredient ingredientByID = LiveDataTestUtil.getOrAwaitValue(ingredientDao.getIngredientByID(id));
        assertEquals(ingredientName, ingredientByID.getName());

        //Update Ingredient
        String updateName = "Candy Cane";
        Ingredient ingredientUpdate = new Ingredient();
        ingredientUpdate.setId(1);
        ingredientUpdate.setName(updateName);
        int updated = ingredientDao.updateIngredient(ingredientUpdate);
        assertEquals(1, updated);

        //Get IngredientByName
        LiveData<Ingredient> ingredientByName = ingredientDao.getIngredientByName(updateName);
        assertEquals(updateName, LiveDataTestUtil.getOrAwaitValue(ingredientByName).getName());

        //Get IngredientsWithNamesLike
        String like = "ch";
        List<Ingredient> ingredientsWithNamesLike = LiveDataTestUtil.getOrAwaitValue(ingredientDao.getIngredientsWithNamesLike(like));
        for(Ingredient ingredientFromList: ingredientsWithNamesLike){
            assertTrue(ingredientFromList.getName().contains(like));
        }

        //Get IngredientsStartingWith
        String startWith = "Ki";
        List<Ingredient> ingredientsStartingWith = LiveDataTestUtil.getOrAwaitValue(ingredientDao.getIngredientsStartingWith(startWith));
        for(Ingredient ingredientFromList: ingredientsStartingWith){
            assertTrue(ingredientFromList.getName().startsWith(startWith));
        }

        //Get AllIngredients
        List<Ingredient> allIngredients = LiveDataTestUtil.getOrAwaitValue(ingredientDao.getAllIngredients());
        assertEquals(3, allIngredients.size());
        for(int i =1; i<allIngredients.size(); i++){
            assertTrue(allIngredients.get(i-1).getId() < allIngredients.get(i).getId());
        }

        //Get AllIngredientsAlphabetized
        List<Ingredient> allIngredientsAlphabetized = LiveDataTestUtil.getOrAwaitValue(ingredientDao.getAlphabetizedIngredients());
        assertEquals(3, allIngredientsAlphabetized.size());
        for(int i =1; i<allIngredientsAlphabetized.size(); i++){
            assertTrue(allIngredientsAlphabetized.get(i-1).getName().compareTo(allIngredientsAlphabetized.get(i).getName()) <=0);
        }

        //Delete Ingredient
        ingredientDao.deleteIngredient(ingredientByID);
        assertNull(LiveDataTestUtil.getOrAwaitValue(ingredientDao.getIngredientByID(ingredientByID.getId())));

        //Delete IngredientById
        ingredientDao.deleteIngredientById(allIngredients.get(1).getId());
        assertNull(LiveDataTestUtil.getOrAwaitValue(ingredientDao.getIngredientByID(allIngredients.get(1).getId())));

        //Delete all Ingredients
        ingredientDao.deleteAllIngredients();
        assertEquals(0, LiveDataTestUtil.getOrAwaitValue(ingredientDao.getAllIngredients()).size());
    }

	@Test
    public void manageRecipes() throws Exception{
        //Insert Recipe
        String recipeName = "Gochujang Chicken";
        int recipeTime = 20;
        String recipeDescription = "A spicy chicken dish hailing from Korea.";
        Recipe recipe = new Recipe();
        recipe.setName(recipeName);
        recipe.setTime(recipeTime);
        recipe.setDescription(recipeDescription);

        long id = recipeDao.insertRecipe(recipe);
        assertNotEquals(-1, id);

        //Get RecipeById
        LiveData<Recipe> recipeById = recipeDao.getRecipeById(id);
        assertEquals(id, LiveDataTestUtil.getOrAwaitValue(recipeById).getId());

        //Insert Recipes
        String recipeName2 = "Grilled Cheese";
        int recipeTime2 = 5;
        String recipeDescription2 = "A simple sandwich.";
        Recipe recipe2 = new Recipe();
        recipe2.setName(recipeName2);
        recipe2.setTime(recipeTime2);
        recipe2.setDescription(recipeDescription2);

        String recipeName3 = "Mac N' Cheese";
        int recipeTime3 = 45;
        String recipeDescription3 = "A cheesy pasta dish.";
        Recipe recipe3 = new Recipe();
        recipe3.setName(recipeName3);
        recipe3.setTime(recipeTime3);
        recipe3.setDescription(recipeDescription3);

        String recipeName4 = "Grilled Chicken";
        int recipeTime4 = 45;
        String recipeDescription4 = "A chicken dish";
        Recipe recipe4 = new Recipe();
        recipe4.setName(recipeName4);
        recipe4.setTime(recipeTime4);
        recipe4.setDescription(recipeDescription4);

        Recipe[] recipes = {recipe2, recipe3, recipe4};
        List<Long> ids = recipeDao.insertRecipes(recipes);
        assertEquals(3, ids.size());

        //Get RecipesById
        long[] idsAsArray = new long[3];
        for(int i =0; i <ids.size(); i++){
            idsAsArray[i] = ids.get(i);
        }
        List<Recipe> recipesById = LiveDataTestUtil.getOrAwaitValue(recipeDao.getRecipesById(idsAsArray));
        for(int i = 0; i<recipesById.size(); i++){
            assertEquals(idsAsArray[i], recipesById.get(i).getId());
        }

        //Update Recipe
        Recipe updatedRecipe = new Recipe();
        updatedRecipe.setId(id);
        updatedRecipe.setDescription(recipeDescription);
        updatedRecipe.setName(recipeName);
        updatedRecipe.setTime(55);

        int updated = recipeDao.updateRecipe(updatedRecipe);
        assertNotEquals(-1, updated);

        //Get AllRecipes
        List<Recipe> allRecipes = LiveDataTestUtil.getOrAwaitValue(recipeDao.getAllRecipes());
        assertEquals(4, allRecipes.size());
        for(int i = 1; i <allRecipes.size(); i++){
            assertTrue(allRecipes.get(i-1).getId() < allRecipes.get(i).getId());
        }

        //Get RecipesStartingWithName
        List<Recipe> recipesStartingWithName = LiveDataTestUtil.getOrAwaitValue(recipeDao.getRecipesStartingWithName("G"));
        for(int i = 0; i <recipesStartingWithName.size(); i++){
            assertTrue(recipesStartingWithName.get(i).getName().startsWith("G"));
        }

        //Get RecipesContainingName
        List<Recipe> recipesContainingWithName = LiveDataTestUtil.getOrAwaitValue(recipeDao.getRecipesContainingName("Che"));
        for(int i = 0; i <recipesContainingWithName.size(); i++){
            assertTrue(recipesContainingWithName.get(i).getName().contains("Che"));
        }

        //Get RecipesUsingIngredient
        Ingredient insertIngredient = insertIngredient("Chicken");
        IngredientList ingredientList = insertIngredientList(insertIngredient.getId(), updatedRecipe.getId());


        List<Long> recipeUsingIngredient =  LiveDataTestUtil.getOrAwaitValue(recipeDao.getRecipesUsingIngredient(insertIngredient.getId()));
        assertEquals(updatedRecipe.getId(), (long)recipeUsingIngredient.get(0));


        //Delete Recipe
        recipeDao.deleteRecipe(updatedRecipe);
        assertNull(LiveDataTestUtil.getOrAwaitValue(recipeDao.getRecipeById(updatedRecipe.getId())));

        //Delete Recipes
        recipeDao.deleteRecipes(new Recipe[]{allRecipes.get(1), allRecipes.get(2)});
        assertNull(LiveDataTestUtil.getOrAwaitValue(recipeDao.getRecipeById(allRecipes.get(1).getId())));
        assertNull(LiveDataTestUtil.getOrAwaitValue(recipeDao.getRecipeById(allRecipes.get(2).getId())));

        //Delete AllRecipes
        recipeDao.deleteAll();
        assertEquals(0, LiveDataTestUtil.getOrAwaitValue(recipeDao.getAllRecipes()).size());

        //Cleanup
        deleteIngredient(insertIngredient);
        deleteIngredientList(ingredientList);
    }

	@Test
    public void manageIngredientLists() throws Exception {
        //Insert IngredientList
        String ingredientName = "Chicken";
        Ingredient insertIngredient = insertIngredient(ingredientName);

        String recipeName = "Chicken A La King";
        int recipeTime = 20;
        String recipeDescription = "A creamy chicken dish";
        Recipe insertRecipe = insertRecipe(recipeName, recipeTime, recipeDescription);

        double quantity = 12;
        Measurement measurement = Measurement.g;

        IngredientList ingredientList = new IngredientList();
        ingredientList.setQuantity(quantity);
        ingredientList.setRecipeId(insertRecipe.getId());
        ingredientList.setIngredientId(insertIngredient.getId());
        ingredientList.setMeasurement(measurement);

        ingredientListDao.insertIngredientList(ingredientList);

        //Get IngredientListByRecipeId
        List<IngredientList> ingredientListByRecipeId = LiveDataTestUtil.getOrAwaitValue(ingredientListDao.getIngredientListByRecipeId(insertRecipe.getId()));
        assertEquals(1, ingredientListByRecipeId.size());
        for (IngredientList il : ingredientListByRecipeId) {
            assertEquals(ingredientList.getRecipeId(), il.getRecipeId());
        }

        //Update IngredientList
        IngredientList update = new IngredientList();
        update.setMeasurement(Measurement.dash);
        update.setIngredientId(insertIngredient.getId());
        update.setRecipeId(insertRecipe.getId());
        update.setQuantity(14);
        int updated = ingredientListDao.updateIngredientList(update);
        assertEquals(1, updated);


        //Insert IngredientLists
        String ingredient2Name = "Peas";
        Ingredient insertIngredient2 = insertIngredient(ingredient2Name);
        String ingredient3Name = "Cream of Mushroom Soup";
        Ingredient insertIngredient3 = insertIngredient(ingredient3Name);
        String ingredient4Name = "Carrots";
        Ingredient insertIngredient4 = insertIngredient(ingredient4Name);

        double quantity2 = 13;
        Measurement measurement2 = Measurement.cup;
        IngredientList ingredientList2 = new IngredientList();
        ingredientList2.setQuantity(quantity2);
        ingredientList2.setRecipeId(insertRecipe.getId());
        ingredientList2.setIngredientId(insertIngredient2.getId());
        ingredientList2.setMeasurement(measurement2);

        double quantity3 = 23;
        Measurement measurement3 = Measurement.bundle;
        IngredientList ingredientList3 = new IngredientList();
        ingredientList3.setQuantity(quantity3);
        ingredientList3.setRecipeId(insertRecipe.getId());
        ingredientList3.setIngredientId(insertIngredient3.getId());
        ingredientList3.setMeasurement(measurement3);

        double quantity4 = 26;
        Measurement measurement4 = Measurement.g;
        IngredientList ingredientList4 = new IngredientList();
        ingredientList4.setQuantity(quantity4);
        ingredientList4.setRecipeId(insertRecipe.getId());
        ingredientList4.setIngredientId(insertIngredient4.getId());
        ingredientList4.setMeasurement(measurement4);

        ingredientListDao.insertIngredientLists(new IngredientList[]{ingredientList2, ingredientList3, ingredientList4});
        ingredientListByRecipeId = LiveDataTestUtil.getOrAwaitValue(ingredientListDao.getIngredientListByRecipeId(insertRecipe.getId()));
        assertEquals(4, ingredientListByRecipeId.size());
        for (IngredientList il : ingredientListByRecipeId) {
            assertEquals(ingredientList.getRecipeId(), il.getRecipeId());
        }

        //Delete IngredientList
        ingredientListDao.deleteIngredientList(ingredientList);
        ingredientListByRecipeId = LiveDataTestUtil.getOrAwaitValue(ingredientListDao.getIngredientListByRecipeId(insertRecipe.getId()));
        assertEquals(3, ingredientListByRecipeId.size());
        for (IngredientList il : ingredientListByRecipeId) {
            assertNotEquals(ingredientList.getIngredientId(), il.getIngredientId());
        }

        //Delete IngredientLists
        ingredientListDao.deleteIngredientLists(new IngredientList[]{ingredientList2, ingredientList3});
        ingredientListByRecipeId = LiveDataTestUtil.getOrAwaitValue(ingredientListDao.getIngredientListByRecipeId(insertRecipe.getId()));
        assertEquals(1, ingredientListByRecipeId.size());
        for (IngredientList il : ingredientListByRecipeId) {
            assertNotEquals(ingredientList2.getIngredientId(), il.getIngredientId());
            assertNotEquals(ingredientList3.getIngredientId(), il.getIngredientId());
        }

        //Delete IngredientListByRecipeId
        String recipeName2 = "Tonkotsu Ramen";
        int recipe2Time = 20;
        String recipe2Desc = "A delicious ramen filled with flavourful broth.";
        Recipe insertRecipe2 = insertRecipe(recipeName2, recipe2Time, recipe2Desc);
        IngredientList ingredientList5 = insertIngredientList(insertIngredient.getId(), insertRecipe2.getId());

        ingredientListDao.deleteIngredientListByRecipeId(insertRecipe.getId());
        ingredientListByRecipeId = LiveDataTestUtil.getOrAwaitValue(ingredientListDao.getIngredientListByRecipeId(insertRecipe.getId()));
        assertEquals(0, ingredientListByRecipeId.size());

        //Delete All
        ingredientListDao.deleteAll();
        ingredientListByRecipeId = LiveDataTestUtil.getOrAwaitValue(ingredientListDao.getIngredientListByRecipeId(insertRecipe.getId()));
        assertEquals(0, ingredientListByRecipeId.size());
        ingredientListByRecipeId = LiveDataTestUtil.getOrAwaitValue(ingredientListDao.getIngredientListByRecipeId(insertRecipe2.getId()));
        assertEquals(0, ingredientListByRecipeId.size());


        //Clean Up
        deleteIngredients(insertIngredient, insertIngredient2, insertIngredient3, insertIngredient4);
        deleteRecipes(insertRecipe, insertRecipe2);
    }

    @Test
    public void manageRecipeDirections() throws Exception {
        //Insert RecipeDirections
        String recipeName = "Chicken Pot Pie";
        int recipeTime = 45;
        String recipeDescription = "A creamy pot pie with a phyllo crust.";
        Recipe recipe = insertRecipe(recipeName, recipeTime, recipeDescription);

        RecipeDirections recipeDirections = new RecipeDirections();
        String recipeStepDescription = "Pull out phyllo dough to defrost.";
        int stepNum = 1;
        recipeDirections.setRecipeID(recipe.getId());
        recipeDirections.setRecipeStep(stepNum);
        recipeDirections.setStepDirections(recipeStepDescription);
        recipeDirectionsDao.insertRecipeDirection(recipeDirections);

        //Get RecipeDirectionsByRecipeId
        List<RecipeDirections> recipeDirectionsByRecipeId = LiveDataTestUtil.getOrAwaitValue(recipeDirectionsDao.getRecipeDirectionsByRecipeId(recipe.getId()));
        assertEquals(1, recipeDirectionsByRecipeId.size());
        for(RecipeDirections recipeDirection: recipeDirectionsByRecipeId){
            assertEquals(recipeDirection.getRecipeID(), recipe.getId());
        }

        //Insert RecipeDirections
        String recipeName2 = "Beef Pot Pie";
        int recipeTime2 = 50;
        String recipeDescription2 = "A hearty beef pot pie with a phyllo crust.";
        Recipe recipe2 = insertRecipe(recipeName2, recipeTime2, recipeDescription2);

        RecipeDirections recipeDirections2 = new RecipeDirections();
        String recipeStepDescription2 = "Roll Phyllo crust into a flat sheet.";
        int stepNum2 = 2;
        recipeDirections2.setRecipeID(recipe.getId());
        recipeDirections2.setRecipeStep(stepNum2);
        recipeDirections2.setStepDirections(recipeStepDescription2);
        recipeDirectionsDao.insertRecipeDirection(recipeDirections2);

        RecipeDirections recipeDirections3 = new RecipeDirections();
        String recipeStepDescription3 = "Cook chicken.";
        int stepNum3 = 3;
        recipeDirections3.setRecipeID(recipe.getId());
        recipeDirections3.setRecipeStep(stepNum3);
        recipeDirections3.setStepDirections(recipeStepDescription3);
        recipeDirectionsDao.insertRecipeDirection(recipeDirections3);

        RecipeDirections recipeDirections4 = new RecipeDirections();
        String recipeStepDescription4 = "Cook Beef";
        int stepNum4 = 4;
        recipeDirections4.setRecipeID(recipe2.getId());
        recipeDirections4.setRecipeStep(stepNum4);
        recipeDirections4.setStepDirections(recipeStepDescription4);
        recipeDirectionsDao.insertRecipeDirection(recipeDirections4);

        recipeDirectionsDao.insertRecipeDirections(new RecipeDirections[]{recipeDirections2,recipeDirections3, recipeDirections4});
        recipeDirectionsByRecipeId = LiveDataTestUtil.getOrAwaitValue(recipeDirectionsDao.getRecipeDirectionsByRecipeId(recipe.getId()));
        assertEquals(3, recipeDirectionsByRecipeId.size());
        for(RecipeDirections recipeDirection: recipeDirectionsByRecipeId){
            assertEquals(recipeDirection.getRecipeID(), recipe.getId());
        }
        recipeDirectionsByRecipeId = LiveDataTestUtil.getOrAwaitValue(recipeDirectionsDao.getRecipeDirectionsByRecipeId(recipe2.getId()));
        assertEquals(1, recipeDirectionsByRecipeId.size());
        for(RecipeDirections recipeDirection: recipeDirectionsByRecipeId){
            assertEquals(recipeDirection.getRecipeID(), recipe2.getId());
        }

        //Update RecipeDirection
        RecipeDirections updatedDirection = new RecipeDirections();
        updatedDirection.setRecipeID(recipe.getId());
        updatedDirection.setRecipeStep(stepNum);
        String updatedDescription = "Defrost the phyllo for 20 minutes.";
        updatedDirection.setStepDirections(updatedDescription);
        int updated = recipeDirectionsDao.updateRecipeDirection(updatedDirection);

        assertEquals(1, updated);

        //Delete RecipeDirection
        recipeDirectionsDao.deleteRecipeDirection(recipeDirections);
        recipeDirectionsByRecipeId = LiveDataTestUtil.getOrAwaitValue(recipeDirectionsDao.getRecipeDirectionsByRecipeId(recipe.getId()));
        assertEquals(2, recipeDirectionsByRecipeId.size());
        for(RecipeDirections recipeDirection: recipeDirectionsByRecipeId){
            assertEquals(recipeDirection.getRecipeID(), recipe.getId());
        }

        //Delete RecipeDirectionsByRecipeId
        recipeDirectionsDao.deleteRecipeDirectionsByRecipeId(recipe2.getId());
        recipeDirectionsByRecipeId = LiveDataTestUtil.getOrAwaitValue(recipeDirectionsDao.getRecipeDirectionsByRecipeId(recipe2.getId()));
        assertEquals(0, recipeDirectionsByRecipeId.size());

        //Delete AllRecipeDirections
        recipeDirectionsDao.deleteAllRecipeDirections();
        recipeDirectionsByRecipeId = LiveDataTestUtil.getOrAwaitValue(recipeDirectionsDao.getRecipeDirectionsByRecipeId(recipe.getId()));
        assertEquals(0, recipeDirectionsByRecipeId.size());

        //Cleanup
        deleteRecipes(recipe, recipe2);
    }

    @Test
    public void manageRecipeCategories() throws Exception {
        //Insert RecipeCategories
        String recipeName = "Chicken Stir Fry";
        int recipeTime = 20;
        String recipeDescription = "A gingery chicken stir fry";
        Recipe recipe = insertRecipe(recipeName, recipeTime, recipeDescription);

        Category category = Category.Chicken;
        RecipeCategories recipeCategories = new RecipeCategories();
        recipeCategories.setCategory(category);
        recipeCategories.setRecipeId(recipe.getId());
        recipeCategoriesDao.insertRecipeCategories(recipeCategories);

        //Get RecipeCategoriesByRecipeId
        List<RecipeCategories> recipeCategoriesByRecipeId = LiveDataTestUtil.getOrAwaitValue(recipeCategoriesDao.getRecipeCategoriesByRecipeId(recipe.getId()));
        assertEquals(1, recipeCategoriesByRecipeId.size());
        for(RecipeCategories recipeCategory : recipeCategoriesByRecipeId){
            assertEquals(recipe.getId(), recipeCategory.getRecipeId());
        }

        //Insert RecipeCategories (List)
        String recipeName2 = "Beef Stir Fry";
        int recipeTime2 = 30;
        String recipeDescription2 = "A garlicy beef stir fry";
        Recipe recipe2 = insertRecipe(recipeName2, recipeTime2, recipeDescription2);

        Category category2 = Category.Main;
        RecipeCategories recipeCategories2 = new RecipeCategories();
        recipeCategories2.setCategory(category2);
        recipeCategories2.setRecipeId(recipe.getId());

        Category category3 = Category.Other;
        RecipeCategories recipeCategories3 = new RecipeCategories();
        recipeCategories3.setCategory(category3);
        recipeCategories3.setRecipeId(recipe.getId());

        Category category4 = Category.Beef;
        RecipeCategories recipeCategories4 = new RecipeCategories();
        recipeCategories4.setCategory(category4);
        recipeCategories4.setRecipeId(recipe2.getId());

        recipeCategoriesDao.insertRecipeCategories(new RecipeCategories[] {recipeCategories2, recipeCategories3, recipeCategories4});

        recipeCategoriesByRecipeId = LiveDataTestUtil.getOrAwaitValue(recipeCategoriesDao.getRecipeCategoriesByRecipeId(recipe.getId()));
        assertEquals(3, recipeCategoriesByRecipeId.size());
        for(RecipeCategories recipeCategory : recipeCategoriesByRecipeId){
            assertEquals(recipe.getId(), recipeCategory.getRecipeId());
        }
        recipeCategoriesByRecipeId = LiveDataTestUtil.getOrAwaitValue(recipeCategoriesDao.getRecipeCategoriesByRecipeId(recipe2.getId()));
        assertEquals(1, recipeCategoriesByRecipeId.size());
        for(RecipeCategories recipeCategory : recipeCategoriesByRecipeId){
            assertEquals(recipe2.getId(), recipeCategory.getRecipeId());
        }

        //Delete RecipeCategories
        recipeCategoriesDao.deleteRecipeCategories(recipeCategories);

        recipeCategoriesByRecipeId = LiveDataTestUtil.getOrAwaitValue(recipeCategoriesDao.getRecipeCategoriesByRecipeId(recipe.getId()));
        assertEquals(2, recipeCategoriesByRecipeId.size());
        for(RecipeCategories recipeCategory : recipeCategoriesByRecipeId){
            assertNotEquals(recipeCategories.getCategory(), recipeCategory.getCategory());
        }

        //Delete RecipeCategoriesByRecipeId
        recipeCategoriesDao.deleteAllRecipeCategoriesByRecipeId(recipe.getId());

        recipeCategoriesByRecipeId = LiveDataTestUtil.getOrAwaitValue(recipeCategoriesDao.getRecipeCategoriesByRecipeId(recipe.getId()));
        assertEquals(0, recipeCategoriesByRecipeId.size());

        //Delete AllRecipeCategories
        recipeCategoriesDao.deleteAllRecipeCategories();

        recipeCategoriesByRecipeId = LiveDataTestUtil.getOrAwaitValue(recipeCategoriesDao.getRecipeCategoriesByRecipeId(recipe2.getId()));
        assertEquals(0, recipeCategoriesByRecipeId.size());

        //Clean Up
        deleteRecipes(recipe, recipe2);
    }


    //Helper Methods
    private Ingredient insertIngredient(String name) throws InterruptedException {
        Ingredient ingredient = new Ingredient();
        ingredient.setName(name);
        long id = ingredientDao.insertIngredient(ingredient);

        return LiveDataTestUtil.getOrAwaitValue(ingredientDao.getIngredientByID(id));
    }

    private void deleteIngredient(Ingredient ingredient){
        ingredientDao.deleteIngredient(ingredient);
    }

    private void deleteIngredients(Ingredient ... ingredients){
        for(Ingredient ingredient : ingredients){
            deleteIngredient(ingredient);
        }
    }

    private IngredientList insertIngredientList(long ingredientID, long recipeId){
        IngredientList il = new IngredientList();
        il.setIngredientId(ingredientID);
        il.setRecipeId(recipeId);
        il.setMeasurement(Measurement.lb);
        il.setQuantity(1);
        ingredientListDao.insertIngredientList(il);
        return il;
    }

    private void deleteIngredientList(IngredientList il){
        ingredientListDao.deleteIngredientList(il);
    }

    private Recipe insertRecipe(String recipeName, int recipeTime, String recipeDescription) throws InterruptedException {
        Recipe recipe = new Recipe();
        recipe.setName(recipeName);
        recipe.setTime(recipeTime);
        recipe.setDescription(recipeDescription);
        long id =  recipeDao.insertRecipe(recipe);

        return LiveDataTestUtil.getOrAwaitValue(recipeDao.getRecipeById(id));
    }

    private void deleteRecipe(Recipe recipe){
        recipeDao.deleteRecipe(recipe);
    }

    private void deleteRecipes(Recipe ... recipes){
        for(Recipe recipe : recipes){
            deleteRecipe(recipe);
        }
    }
}
